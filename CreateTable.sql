USE Twitter2;

Drop Table IF Exists [Follower];
Drop Table IF Exists [Friend];
Drop Table IF Exists [Tweet];
Drop Table IF Exists [User];

CREATE TABLE [User](
	UserID nvarchar(50) NOT NULL,
	Name nvarchar(50) NULL,
	ScreenName nvarchar(50) NULL,
	Description nvarchar(max) NULL,
	Location nvarchar(50) NULL,
	CreatedAt datetime NULL,
	Friends int NULL,
	Followers int NULL,
	Tweets int NULL,
	TwitterURL nvarchar(max) NULL,
	PRIMARY KEY (UserID))
	/*CONSTRAINT PK_User PRIMARY KEY (UserID))*/

CREATE TABLE [Follower](
	ID INT IDENTITY(1,1) NOT NULL,
	UserID nvarchar(50) NOT NULL,
	FollowerID nvarchar(50) NOT NULL,
	Name nvarchar(max) NULL,
	ScreenName nvarchar(50) NULL,
	PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES [User](UserID))
	/*CONSTRAINT PK_Follower PRIMARY KEY (ID))*/

CREATE TABLE [Friend](
	ID INT IDENTITY(1,1) NOT NULL,
	UserID nvarchar(50) NOT NULL,
	FriendID nvarchar(50) NOT NULL,
	Name nvarchar(max) NULL,
	ScreenName nvarchar(50) NULL,
	PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES [User](UserID))
	/*CONSTRAINT PK_Friend PRIMARY KEY (ID))*/

CREATE TABLE [Tweet](
	ID INT IDENTITY(1,1) NOT NULL,
	TweetID nvarchar(50) NOT NULL,
	UserID nvarchar(50) NOT NULL,
	Keyword nvarchar(50) NULL,
	Content nvarchar(max) NULL,
	Likes int NULL,
	Retweets int NULL,
	CreatedAt datetime NULL,
	TweetURL nvarchar(max) NULL,
	PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES [User](UserID));
	/*
	CONSTRAINT PK_Tweet PRIMARY KEY (TweetID))
    CONSTRAINT FK_User_Tweet FOREIGN KEY (TwitterID)
    REFERENCES [User](TwitterID));
	*/


