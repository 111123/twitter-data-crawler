#from typing import Counter
#from urllib.parse import uses_relative
import tweepy
import pandas as pd
import pyodbc

#MSSQL Connect
conn = pyodbc.connect("Driver={SQL Server};"
                      "Server=LAPTOP-3J1JV6PG\MSSQLSERVER02;"
                      "Database=Twitter2;"
                      "Trusted_Connection=yes;"
                      "ansi=True,autocommit=True")

cursor = conn.cursor()

# get the key to use the api
consumer_key = "K1Tl5nvHMlOSuVnDrm3wkaEcn"
consumer_secret = "PA8NBiJa0e05gm0GomFDF7u4XTFh8QyymkmtbqTwnIFpiiPYos"
access_token = "1386488982838530049-gbLhyxusvH8sj5262M4fPdj9sYEVBR"
access_token_secret = "bt37k0v13HAvbzJnlD5EIrTsMWyunWSHCkQwZchiZtngb"

#set the key
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True)

#------------------------------------------------------------------------------------------------------------

num_Of_Items = 10 # search how many items?

#get JoeBiden info
user = api.get_user("JoeBiden")

#Delete Table Data-------------------------------------------------------------------------------------------
def delete_Table_Data():   
    cursor.execute("DELETE FROM [Friend];")
    cursor.execute("DELETE FROM [Follower];")
    cursor.execute("DELETE FROM [Tweet];")
    cursor.execute("DELETE FROM [User];") 
    conn.commit()
    print("Delete Data: OK!") 

#Check Table Function----------------------------------------------------------------------------------------

#Check is there are a record inside the User table
def check_Data_In_UserTable(user_ID):
    cursor.execute("SELECT * FROM [User] WHERE [UserID] = '{}';".format(user_ID))
    result = cursor.fetchone()
    if result is None:
        return 0
    else:
        return 1

#Check is there are a record inside the Friend table
def check_Data_In_FirendTable(user_ID):
    cursor.execute("SELECT * FROM [Friend] WHERE [UserID] = '{}';".format(user_ID))
    result = cursor.fetchone()
    if result is None:
        return 0
    else:
        return 1

#Check is there are a record inside the Follower table
def check_Data_In_FollowerTable(user_ID):
    cursor.execute("SELECT * FROM [Follower] WHERE [UserID] = '{}';".format(user_ID))
    result = cursor.fetchone()
    if result is None:
        return 0
    else:
        return 1

#Check is there are a record inside the Tweet table
def check_Data_In_TweetTable(tweet_ID):
    cursor.execute("SELECT * FROM [Tweet] WHERE [TweetID] = '{}';".format(tweet_ID))
    result = cursor.fetchone()
    if result is None:
        return 0
    else:
        return 1

#Insert Table Function----------------------------------------------------------------------------------------

def insert_Into_User_Table(user_ID):
    if check_Data_In_UserTable(user_ID) == 0:
        user = api.get_user(user_ID) 
        cursor.execute("INSERT INTO [User] VALUES ('{}',N'{}','{}',N'{}',N'{}','{}','{}','{}','{}','https://twitter.com/{}');"
                            .format(user.id,
                                    user.name.replace("'","''"),
                                    user.screen_name,
                                    user.description.replace("'","''"),
                                    user.location.replace("'","''"),
                                    user.created_at,
                                    user.friends_count,
                                    user.followers_count,                      
                                    user.statuses_count,
                                    user.screen_name))
        conn.commit()

    elif check_Data_In_UserTable(user_ID) == 1:
        print("*****{} is already inside the User Table*****".format(api.get_user(user_ID).screen_name))

def insert_Into_Friend_Table(user_ID, num_of_items):
    user = api.get_user(user_ID)
    count = 0
    print("Finding {} Friends......".format(user.screen_name))
    for friend in tweepy.Cursor(api.friends, user.screen_name).items(num_of_items):
        count += 1
        print("No. {} ------ {}".format(count, friend.screen_name))
        cursor.execute("INSERT INTO [Friend] VALUES ('{}','{}',N'{}','{}');"
                        .format(user.id,
                                friend.id,   
                                friend.name.replace("'","''"),
                                friend.screen_name))

        if check_Data_In_UserTable(friend.id) == 0:
            insert_User_Profile(friend.id)
        elif check_Data_In_UserTable(friend.id) == 1:
            print("*****{} is already inside User Table*****".format(friend.screen_name))
            continue

    conn.commit()

    print("-----" * 20)
    print("Insert Friend Data: OK!")   

def insert_Into_Follower_Table(user_ID, num_of_items):
    user = api.get_user(user_ID)
    count = 0
    print("Finding {} Followers......".format(user.screen_name))
    for follower in tweepy.Cursor(api.followers, user.screen_name).items(num_of_items):
        count += 1
        print("No. {} ------ {}".format(count, follower.screen_name))
        cursor.execute("INSERT INTO [Follower] VALUES ('{}','{}',N'{}','{}');"
                        .format(user.id,
                                follower.id,
                                follower.name.replace("'","''"),
                                follower.screen_name))

        if check_Data_In_UserTable(follower.id) == 0:
            insert_User_Profile(follower.id)
        elif check_Data_In_UserTable(follower.id) == 1:
            print("*****{} is already inside User Table*****".format(follower.screen_name))
            continue

    conn.commit()

    print("-----" * 20)
    print("Insert Follower Data: OK!") 

#-----------------------------------------------------------------------------------------------------------------------

#get user info and insert it to Table User
def insert_JoeBiden_Profile():
    print("Search User")
    print("-----" * 20)
    print("Screen Name: {}".format(user.screen_name),
          "\nName: {}".format(user.name.replace("'","''")),
          "\nDescription: {}".format(user.description.replace("'","''")),
          "\nLocation: {} \nJoined: {}".format(user.location.replace("'","''"), user.created_at),
          "\nFollowing: {} | Followers: {}".format(user.friends_count, user.followers_count))

    if check_Data_In_UserTable(user.id) == 0:
        cursor.execute("INSERT INTO [User] VALUES ('{}','{}','{}',N'{}','{}','{}','{}','{}','{}','https://twitter.com/{}');"
                    .format(user.id,
                            user.name.replace("'","''"),
                            user.screen_name,
                            user.description.replace("'","''"),
                            user.location.replace("'","''"),
                            user.created_at,
                            user.friends_count,
                            user.followers_count,                      
                            user.statuses_count,
                            user.screen_name))
        conn.commit()
        print("-----" * 20)
        print("Insert Joe Biden Data: OK!")

    elif check_Data_In_UserTable(user.id) == 1:
        print("*****{} is already inside User Table*****".format(user.screen_name))



#get user info and insert it to Table User
def insert_User_Profile(user_ID):
    user = api.get_user(user_ID) 
    cursor.execute("INSERT INTO [User] VALUES ('{}',N'{}','{}',N'{}','{}','{}','{}','{}','{}','https://twitter.com/{}');"
                        .format(user.id,
                                user.name.replace("'","''"),
                                user.screen_name,
                                user.description.replace("'","''"),
                                user.location.replace("'","''"),
                                user.created_at,
                                user.friends_count,
                                user.followers_count,                      
                                user.statuses_count,
                                user.screen_name))
    conn.commit()

#get friend info and insert it to Friend Table
def insert_JoeBiden_Friends_Data():
    print("Finding {} Friends......".format(user.screen_name))
    for friend in tweepy.Cursor(api.friends, user.screen_name).items(num_Of_Items):
        print(friend.screen_name)
        cursor.execute("INSERT INTO [Friend] VALUES ('{}','{}',N'{}','{}');"
                        .format(user.id,
                                friend.id,   
                                friend.name.replace("'","''"),
                                friend.screen_name))

        if check_Data_In_UserTable(friend.id) == 0:
            insert_User_Profile(friend.id)
        elif check_Data_In_UserTable(friend.id) == 1:
            print("*****{} is already inside User Table*****".format(friend.screen_name))
            continue

    conn.commit()

    print("-----" * 20)
    print("Insert Friend Data: OK!")

#get follower info and insert it to Follower Table
def insert_JoeBiden_Follower_Data():
    print("Finding {} Followers......".format(user.screen_name))
    for follower in tweepy.Cursor(api.followers, user.screen_name).items(num_Of_Items):
        print(follower.screen_name)
        cursor.execute("INSERT INTO [Follower] VALUES ('{}','{}',N'{}','{}');"
                        .format(user.id,
                                follower.id,
                                follower.name.replace("'","''"),
                                follower.screen_name))

        if check_Data_In_UserTable(follower.id) == 0:
            insert_User_Profile(follower.id)
        elif check_Data_In_UserTable(follower.id) == 1:
            print("*****{} is already inside User Table*****".format(follower.screen_name))
            continue

    conn.commit()

    print("-----" * 20)
    print("Insert Follower Data: OK!")

#get Coronavirus tweet and insert it to Tweet Table
def find_Coronavirus_Tweet():
    print("Finding Coronavirus Tweet......")
    count = 0
    for tweet in tweepy.Cursor(api.search, q="Coronavirus").items(num_Of_Items):
        insert_Into_User_Table(tweet.user.id)
        count += 1

        try:
            favorite_count = tweet.retweeted_status.favorite_count
            created_at = tweet.retweeted_status.created_at
        except:
            favorite_count = 0
            created_at = tweet.created_at
        
        print("\nNo.", count, ">" * 10)
        print("\n",tweet.text,
              "\n\nCreatedAt:", created_at,
              " | Likes:", favorite_count,
              " | Retweet:", tweet.retweet_count)
        print("-----" * 20)

        try:
            cursor.execute("INSERT INTO [Tweet] VALUES ('{}','{}','{}',N'{}','{}','{}','{}','https://twitter.com/twitter/statuses/{}');"
                            .format(tweet.id,
                                    tweet.user.id,
                                    "Coronavirus",
                                    tweet.text.replace("'","''"),
                                    favorite_count,
                                    tweet.retweet_count,
                                    created_at, 
                                    tweet.id))
            conn.commit()
        except:
            continue  

    print("Insert Coronavirus Tweet Data: OK!")

#get Vaccination tweet and insert it to Tweet Table
def find_Vaccination_Tweet():
    count = 0
    print("Finding Vaccination Tweet......")
    for tweet in tweepy.Cursor(api.search, q="Vaccination").items(num_Of_Items):
        insert_Into_User_Table(tweet.user.id)
        count += 1

        try:
            favorite_count = tweet.retweeted_status.favorite_count
            created_at = tweet.retweeted_status.created_at
        except:
            favorite_count = 0
            created_at = tweet.created_at

        print("\nNo.", count, ">" * 10)
        print("\n",tweet.text,
              "\n\nCreatedAt:", created_at,
              " | Likes:", favorite_count,
              " | Retweet:", tweet.retweet_count)
        print("-----" * 20)
        try:
            cursor.execute("INSERT INTO [Tweet] VALUES ('{}','{}','{}',N'{}','{}','{}','{}','https://twitter.com/twitter/statuses/{}');"
                            .format(tweet.id,
                                    tweet.user.id,
                                    "Vaccination",
                                    tweet.text.replace("'","''"),
                                    favorite_count,
                                    tweet.retweet_count,
                                    created_at,
                                    tweet.id))
            conn.commit() 
        except:
            continue

    print("Insert Vaccination Tweet Data: OK!")

# Use user input key word to find a tweet
def find_By_User_Input(key_To_Search, num_Of_Tweet):
    print("-----" * 20)
    print("Finding......")
    count = 0
    tweet_ID = []
    for tweet in tweepy.Cursor(api.search, q=key_To_Search).items(num_Of_Tweet):
        count += 1
        tweet_ID.append(tweet.id)
        try:
            favorite_count = tweet.retweeted_status.favorite_count
            created_at = tweet.retweeted_status.created_at
        except:
            favorite_count = 0
            created_at = tweet.created_at
        print("\nNo.", count, ">" * 10)
        print("\n",tweet.text,
              "\n\nCreatedAt:", created_at,
              " | Likes:", favorite_count,
              " | Retweet:", tweet.retweet_count)
        print("-----" * 20)

    yes_No = input("Insert these tweets to the Database? (y=Yes, n=No, 0=Exit): ")
    if yes_No.lower() == "y":
        print("Inserting to the database......")
        for id in tweet_ID:
            tweet = api.get_status(id)
            insert_Into_User_Table(tweet.user.id)
            try:
                favorite_count = tweet.retweeted_status.favorite_count
                created_at = tweet.retweeted_status.created_at
            except:
                favorite_count = 0
                created_at = tweet.created_at
            try:
                cursor.execute("INSERT INTO [Tweet] VALUES ('{}','{}','{}',N'{}','{}','{}','{}','https://twitter.com/twitter/statuses/{}');"
                            .format(tweet.id,
                                    tweet.user.id,
                                    key_To_Search,
                                    tweet.text.replace("'","''"),
                                    favorite_count,
                                    tweet.retweet_count,
                                    created_at,
                                    tweet.id))
                conn.commit()
            except:
                continue

        print("-----" * 20)
        print("Insert {} Tweet Data: OK!".format(key_To_Search))
        find_Tweet_Function()
    elif yes_No.lower() == "n":
        print("-----" * 20)
        print("Go Back")
        find_Tweet_Function()
    elif yes_No == "0":
        print("-----" * 20)
        print("Go Back to Main Function")
        print("-----" * 20)
        main_Function()

#------------------------------------------------------------------------------------------------------------

# show Tweet table
def show_Tweet_Table(keyword = None):
    if keyword is None:
        sql_query = pd.read_sql_query("SELECT TOP (50) [ID],[TweetID],[UserID],[Keyword],[Content],[Likes],[Retweets],DATEADD(hh,8,CreatedAt) AS CreatedAt,[TweetURL] "
                                      "FROM [Tweet] "
                                      "ORDER BY [Likes] DESC, DATEADD(hh,8,CreatedAt) DESC;", conn)
    else:
        sql_query = pd.read_sql_query("SELECT TOP (50) [ID],[TweetID],[UserID],[Keyword],[Content],[Likes],[Retweets],DATEADD(hh,8,CreatedAt) AS CreatedAt,[TweetURL] "
                                      "FROM [Tweet] "
                                      "WHERE [Keyword] LIKE '%{}%' "
                                      "ORDER BY [Likes] DESC, DATEADD(hh,8,CreatedAt) DESC;"
                                      .format(keyword), conn)
    df = pd.DataFrame(sql_query)
    # pd.set_option('display.max_rows', 1000)
    # pd.set_option('display.max_columns', 100)
    #pd.set_option('display.width', 500)
    print(df)

# show JoeBiden info form User table
def show_JoeBiden_Table():
    sql_query = pd.read_sql_query("SELECT * FROM [User] WHERE [UserID] = '939091'; ", conn)
    df = pd.DataFrame(sql_query)
    print(df)

# show User table
def show_User_Table(user_name = None):
    if user_name is None:
        sql_query = pd.read_sql_query("SELECT TOP 50 * FROM [User] ORDER BY [Followers] DESC; ", conn)
    else:
        sql_query = pd.read_sql_query("SELECT TOP 50 * FROM [User] "
                                      "WHERE [Name] LIKE '%{}%' "
                                      "ORDER BY [Followers] DESC;".format(user_name), conn)

    df = pd.DataFrame(sql_query)
    #df.fillna("Nothing")
    print(df)

# show Friend table
def show_Friend_Table(user_name = None):
    if user_name is None:
        sql_query = pd.read_sql_query("SELECT f.[ID],f.[UserID],u.[ScreenName],f.[FriendID],f.[Name],f.[ScreenName] "
                                      "FROM [Friend] AS f, [User] AS u "
                                      "WHERE f.[UserID] = u.[UserID];", conn)
    else:
        sql_query = pd.read_sql_query("SELECT f.[ID],f.[UserID],u.[ScreenName],f.[FriendID],f.[Name],f.[ScreenName] "
                                      "FROM [Friend] AS f, [User] AS u "
                                      "WHERE f.[UserID] = u.[UserID] AND u.[ScreenName] LIKE '%{}%';".format(user_name), conn)
    #sql_query = pd.read_sql_query("SELECT * FROM [Friend]; ", conn)
    df = pd.DataFrame(sql_query)
    print(df)

# show Follower table
def show_Follower_Table(user_name = None):
    if user_name is None:
        sql_query = pd.read_sql_query("SELECT f.[ID],f.[UserID],u.[ScreenName],f.[FollowerID],f.[Name],f.[ScreenName] "
                                      "FROM [Follower] AS f, [User] AS u "
                                      "WHERE f.[UserID] = u.[UserID];", conn)
    else:
        sql_query = pd.read_sql_query("SELECT f.[ID],f.[UserID],u.[ScreenName],f.[FollowerID],f.[Name],f.[ScreenName] "
                                      "FROM [Follower] AS f, [User] AS u "
                                      "WHERE f.[UserID] = u.[UserID] AND u.[ScreenName] LIKE '%{}%';".format(user_name), conn)
    #sql_query = pd.read_sql_query("SELECT * FROM [Follower]; ", conn)
    df = pd.DataFrame(sql_query)
    print(df)

#--------------------------------------------------------------------------------------------------------------

def find_Tweet_Function():
    print("-----" * 20)
    print("Search Tweet")
    print("-----" * 20)
    key_To_Search = input("Please input a keyword to find the tweet(e.g. Coronavirus, Vaccination)(0=Exit): ")
    if key_To_Search == "0":
        print("-----" * 20)
        print("Go Back to Main Function")
        print("-----" * 20)
        main_Function()
    num_Of_Tweet = input("How many tweets do you want?(0=Exit): ")
    if num_Of_Tweet == "0":
        print("-----" * 20)
        print("Go Back to Main Function")
        print("-----" * 20)
        main_Function()
    find_By_User_Input(key_To_Search, int(num_Of_Tweet))

def find_Multiple_User():
    print("-----" * 20)
    print("Search User")
    print("-----" * 20)
    user_name = input("Please input a name to find a user: (0:Exit) ")
    if user_name == "0":
        main_Function()
    # search the query
    users = api.search_users(user_name, 5)
    count = 0
    users_list = []
    # print the users retrieved
    print("-----" * 20)
    print("Search User")
    for user in users:

        count += 1
        users_list.append(user.id)

        print("-----" * 20)
        print("No.", count, ">" * 10)
        print("-----" * 20)
        print("Screen Name: {}".format(user.screen_name),
              "\nName: {}".format(user.name.replace("'","''")),
              "\nDescription: {}".format(user.description.replace("'","''")),
              "\nLocation: {} \nJoined: {}".format(user.location.replace("'","''"), user.created_at),
              "\nFollowing: {} | Followers: {}".format(user.friends_count, user.followers_count))
        print("-----" * 20)
    #print(users_list)
    user_input = input("Input a number to choose the user: (0:Exit) ")

    if user_input == "0":
        find_Multiple_User()

    user_id = users_list[int(user_input)-1]
    user = api.get_user(user_id)
    insert_Into_User_Table(user_id)
    print("Insert {} information to the database: OK!".format(user.screen_name))
    print("-----" * 20)
    show_User_Table(user.name)
    main_Function3(user_id, user.screen_name)


def show_Table_Function():
    print("-----" * 20)
    print("Which table do you want to show? Tap the number.")
    print("-----" * 20)
    user_choice3 = input("1. User Table"
                         "\n2. Friend Table"
                         "\n3. Follower Table"
                         "\n4. Tweet Table"
                         "\n0. Exit\n")

    if user_choice3 == "1":
        print("-----" * 20)
        print("User Table")
        print("-----" * 20)
        show_choice = input("1. Show All"
                            "\n2. Search & Show"
                            "\n0. Exit\n")
        if show_choice == "0":
            show_Table_Function()

        while show_choice != "0":

            if show_choice == "1":
                show_User_Table()
            elif show_choice == "2":
                show_choice2 = str(input("Input user name to search: "))
                show_User_Table(show_choice2)
            else:
                print("-----" * 20)
                print("Try Again!!")
            print("-----" * 20)
            show_choice = input("1. Show All"
                                "\n2. Search & Show"
                                "\n0. Exit\n")

        show_Table_Function()
    elif user_choice3 == "2":
        print("-----" * 20)
        print("Friend Table")
        print("-----" * 20)
        
        show_choice = input("1. Show All"
                            "\n2. Search & Show"
                            "\n0. Exit\n")
        if show_choice == "0":
            show_Table_Function()

        while show_choice != "0":

            if show_choice == "1":
                show_Friend_Table()
            elif show_choice == "2":
                show_choice2 = str(input("Input user name to search: "))
                show_Friend_Table(show_choice2)
            else:
                print("-----" * 20)
                print("Try Again!!")
            print("-----" * 20)
            show_choice = input("1. Show All"
                                "\n2. Search & Show"
                                "\n0. Exit\n")

        show_Table_Function()     
    elif user_choice3 == "3":
        print("-----" * 20)
        print("Follower Table")
        print("-----" * 20)
        
        show_choice = input("1. Show All"
                            "\n2. Search & Show"
                            "\n0. Exit\n")
        if show_choice == "0":
            show_Table_Function()

        while show_choice != "0":

            if show_choice == "1":
                show_Follower_Table()
            elif show_choice == "2":
                show_choice2 = str(input("Input user name to search: "))
                show_Follower_Table(show_choice2)
            else:
                print("-----" * 20)
                print("Try Again!!")
            print("-----" * 20)
            show_choice = input("1. Show All"
                                "\n2. Search & Show"
                                "\n0. Exit\n")

        show_Table_Function()
    elif user_choice3 == "4":
        print("-----" * 20)
        print("Tweet Table")
        print("-----" * 20)
        show_choice = input("1. Show All"
                            "\n2. Search & Show"
                            "\n0. Exit\n")
        if show_choice == "0":
            show_Table_Function()

        while show_choice != "0":

            if show_choice == "1":
                show_Tweet_Table()
            elif show_choice == "2":
                show_choice2 = str(input("Input key word to search: "))
                show_Tweet_Table(show_choice2)
            else:
                print("-----" * 20)
                print("Try Again!!")
            print("-----" * 20)
            show_choice = input("1. Show All"
                                "\n2. Search & Show"
                                "\n0. Exit\n")

        show_Table_Function()
    elif user_choice3 == "0":
        print("-----" * 20)
        print("Go Back!!")
        print("-----" * 20)
        main_Function()
    else:
        print("-----" * 20)
        print("Try Again!!")
        print("-----" * 20)
        show_Table_Function()      

def main_Function3(user_id, user_screen_name):
    user_id = user_id
    user_screen_name = user_screen_name
    print("-----" * 20)
    user_Choice2 = input("1. Get {} Friends"
                         "\n2. Get {} Followers"
                         "\n0. Go Back to Main Function\n"
                         .format(user_screen_name, user_screen_name))                  
    if user_Choice2 == "1":
        num_of_items = input("How many friends do you want? ")
        print("-----" * 20)
        insert_Into_Friend_Table(user_id, int(num_of_items))
        print("-----" * 20)
        main_Function3(user_id, user_screen_name)      
    elif user_Choice2 == "2":
        num_of_items = input("How many followers do you want? ")
        print("-----" * 20)
        insert_Into_Follower_Table(user_id, int(num_of_items))
        print("-----" * 20)
        main_Function3(user_id, user_screen_name) 
    elif user_Choice2 == "0":
        print("-----" * 20)
        print("Go Back to Main Function")
        print("-----" * 20)
        main_Function()
    else:
        print("-----" * 20)
        print("Try Again!!")
        print("-----" * 20)
        main_Function3(user_id) 

        

def main_Function2():
    user_Choice2 = input("1. Get User's Friends"
                         "\n2. Get User's Followers"
                         "\n0. Go Back to Main Function\n")                   
    if user_Choice2 == "1":
        print("-----" * 20)
        insert_JoeBiden_Friends_Data()
        print("-----" * 20)
        main_Function2()
    elif user_Choice2 == "2":
        print("-----" * 20)
        insert_JoeBiden_Follower_Data()
        print("-----" * 20)
        main_Function2()
    elif user_Choice2 == "0":
        print("-----" * 20)
        print("Go Back to Main Function")
        print("-----" * 20)
        main_Function()
    else:
        print("-----" * 20)
        print("Try Again!!")
        print("-----" * 20)
        main_Function2()

def main_Function():
    print("Welcome to Twitter Crawler!!")
    user_Choice = input("Tap the number to choose the function:"
                        "\n1. Get User Info"
                        "\n2. Find Coronavirus Tweet"
                        "\n3. Find Vaccination Tweet"
                        "\n4. Search Tweet"
                        "\n5. Show Table"
                        "\n6. Delete All Data"
                        "\n0. Exit\n")
    # if user_Choice == "1":
    #     print("-----" * 20)
    #     insert_JoeBiden_Profile()
    #     print("-----" * 20)
    #     show_JoeBiden_Table()
    #     print("-----" * 20)
    #     main_Function2()
    if user_Choice == "1":
        find_Multiple_User()
    elif user_Choice == "2":
        print("-----" * 20)
        find_Coronavirus_Tweet()
        print("-----" * 20)
        main_Function()
    elif user_Choice == "3":
        print("-----" * 20)
        find_Vaccination_Tweet()
        print("-----" * 20)
        main_Function()
    elif user_Choice == "4":
        find_Tweet_Function()  
        print("-----" * 20)  
    elif user_Choice == "5":
        print("-----" * 20)
        show_Table_Function() 
        print("-----" * 20) 
        main_Function()
    elif user_Choice == "6":
        print("-----" * 20)
        delete_Table_Data()
        print("-----" * 20) 
        main_Function()
    elif user_Choice == "0":
        print("-----" * 20)
        print("Goodbye!!")
        print("-----" * 20)
        exit()
    else:
        print("-----" * 20)
        print("Try Again!!")
        print("-----" * 20)
        main_Function()
        

#--------------------------------------------------------------------------------------------------------------

#delete_Table_Data()
main_Function()
#find_Multiple_User()
'''
#tweet = api.get_status("1389151198771437573")
#print(tweet.retweeted_status.favorite_count)

#print user's tweets
public_tweets = api.user_timeline(user.screen_name)
 
for tweet in public_tweets:
    print(tweet.text)
'''
